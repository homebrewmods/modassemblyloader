-- ModAssemblyLoader by RhoSigma
ModAssemblyLoaderBuilderLua = {}

ModAssemblyLoaderBuilderLua.version = "0.1 01/04/2022"

function ModAssemblyLoaderBuilderLua:Awake()

    self.BuilderFieldHelper = Slua.GetClass("ModAssemblyLoader.BuilderFieldHelper")

    self:InjectToInspector()
end

function ModAssemblyLoaderBuilderLua:InjectToInspector()
    if not Inspector then return end
    Inspector.GetWizzardConfigFromTarget = ModAssemblyLoaderBuilderLua.GetWizzardConfigFromTarget
end

-- inspector override to show builder visible fields
function ModAssemblyLoaderBuilderLua:GetWizzardConfigFromTarget(target)
    if not target then return end

    ------------------------------------------------
    --if target is table asume that this is the wizzard config
    ------------------------------------------------
    if type(target) == "table" then
        return target
    end

    ------------------------------------------------

    ------------------------------------------------
    --if target is UnityEngine.GameObject[] then convert to wizzard config
    ------------------------------------------------
    if type(target) == "userdata" and HBU.GetSystemType(target) == "UnityEngine.GameObject[]" then

        --init vars
        local ret = {};
        local groupedParts = {}

        ------------------------------------------------
        --build list of grouped parts
        ------------------------------------------------

        local partGroup = {}
        local sumNameCheck = false
        local canMultiEdit = true
        local gotPart = false
        local partName = "Inspector"

        for selected in Slua.iter(target) do
            if selected and not Slua.IsNull(selected:GetComponent("PartContainer")) then
                local partContainer = selected:GetComponent("PartContainer")

                --get unique name based on proeprties ( if property names and part name match then this name will be thesame )
                local sumName = self:GetPartContainerPropertiesSumName(partContainer)

                --set first sumName
                if not sumNameCheck then sumNameCheck = sumName end

                --if this part does not conform to sumNameCheck then cant set canMultiEdit to false and break
                if sumName ~= sumNameCheck then canMultiEdit = false; break; end

                --if parts conforms to sumNameCheck then add them to the list
                for part in Slua.iter(partContainer.parts) do

                    -- reflected field check
                    local builderFields = ModAssemblyLoaderBuilderLua.BuilderFieldHelper.GetBuilderFields(part)

                    if (not Slua.IsNull(part.properties) and #iter(part.properties) > 0)
                    or (not Slua.IsNull(builderFields) and builderFields.Count > 0) then

                        local haveInspectorWorthyProperties = false
                        for property in Slua.iter(part.properties) do
                            if property and property.hidden == false then haveInspectorWorthyProperties = true end
                        end
                        if builderFields.Count > 0 then haveInspectorWorthyProperties = true end

                        if haveInspectorWorthyProperties then
                            local propertySumName = part:GetPropertiesSumName()
                            if not partGroup[propertySumName] then partGroup[propertySumName] = {} end
                            table.insert(partGroup[propertySumName],part)
                            gotPart = true
                            partName = partContainer.gameObject.name
                        end
                    end
                end
            end
        end

        --dont go further if we dont have any parts in our group
        if not gotPart then return {} end

        ------------------------------------------------

        ------------------------------------------------
        --if we cantmulti edit, then add text property
        ------------------------------------------------

        if not canMultiEdit then
            local item = {
                name = "MultiEditFail",
                uiType = "text",
                layout = {"text","Can't multi-edit unidentical parts, please select individual or identical parts.","min",Vector2(0,30),"textalign",TextAnchor.MiddleCenter},
            }
            table.insert(ret,item)
            return ret
        end

        ------------------------------------------------

        ------------------------------------------------
        --convert part group properties to full property ui config
        ------------------------------------------------

        --create header item
        local headerItem = {
            name         = partName,
            tooltip      = "Part Proprties",
            uiType       = "headerProperty",
            layout       = {"min",Vector2(0,30)}
        }
        table.insert(ret,headerItem)

        --> foreach part in parts group
        for propertySumName,parts in pairs(partGroup) do
            local part = parts[1]
            --local part = partGroup[1]

            --> foreach property of the first part in this group
            for property in Slua.iter(part.properties) do

                --ignore hidden properties
                if property and property.hidden == false then

                    ------------------------------------------------
                    --conver property to property ui config
                    ------------------------------------------------

                    local groupID = part.gameObject.name--"properties"
                    --if property.type == 13 then groupID = "Controls" end
                    --if property.isInput    then groupID = "Inputs"   end
                    --if property.isOutput   then groupID = "Outputs"  end

                    local item = {
                        name          = property.descriptiveName,
                        tooltip       = property.descriptiveName,
                        uiType        = self.PropertyTypeConversion[property.type],
                        minValue      = tonumber(property.minValue) or 0,
                        maxValue      = tonumber(property.maxValue) or 0,
                        options       = iter(property:GetOptions()),
                        groupID       = groupID,
                        matchingValue = self:GetPropertyMatchingValue(parts,property.pname),
                        --setup callback when it asks value ( return the property value of first part )
                        value      = function() return self:PropertyValueConversion(property,"tovalue") end,

                        --setup callback when we change value ( apply value on all parts in this group )
                        func       = function(v)

                        --foreach part >>
                        for i,gpart in ipairs(parts) do
                            if gpart and not Slua.IsNull(gpart) then

                                --property in part that matches this property
                                for prop in Slua.iter(gpart.properties) do
                                    if prop.pname == property.pname then
                                        prop.value = self:PropertyValueConversion(prop,"tostring",v)
                                    end
                                end

                                --read from properties
                                gpart:OnReadFromPropertiesNew()
                                end
                            end

                            --notify builder we changed current assembly
                            HBBuilder.Builder.ChangedCurrentAssembly()
                        end,
                    }

                    ------------------------------------------------

                    ------------------------------------------------
                    --setup array behaviour
                    ------------------------------------------------

                    self:SetupArrayBehaviour(part,property,parts,item)

                    ------------------------------------------------

                    --add to table
                    table.insert(ret,item)
                end
            end

            --------------------------------------------
            -- add part fields with attributes
            --------------------------------------------

            local builderFields = ModAssemblyLoaderBuilderLua.BuilderFieldHelper.GetBuilderFields(part)

            for field in Slua.iter(builderFields) do

                local attrib = ModAssemblyLoaderBuilderLua.BuilderFieldHelper.GetBuilderFieldAttribute(field)

                if field then --and property.hidden == false then

                    ------------------------------------------------
                    --conver property to property ui config
                    ------------------------------------------------

                    local groupID = part.gameObject.name--"properties"
                    --if property.type == 13 then groupID = "Controls" end
                    --if property.isInput    then groupID = "Inputs"   end
                    --if property.isOutput   then groupID = "Outputs"  end

                    local item = {
                        name          = attrib.name,
                        tooltip       = attrib.toolTip or attrib.name,
                        uiType        = ModAssemblyLoaderBuilderLua.FieldTypeConversion[field.FieldType:ToString()],
                        minValue      = tonumber(attrib.minValue) or 0,
                        maxValue      = tonumber(attrib.maxValue) or 0,
                        -- options       = iter(property:GetOptions()),
                        groupID       = groupID,
                        -- matchingValue = self:GetPropertyMatchingValue(parts,property.pname),
                        --setup callback when it asks value ( return the property value of first part )
                        value      = function() return field:GetValue(part) end,

                        -- setup callback when we change value ( apply value on all parts in this group )
                        func       = function(v)
                            -- mutliEdit stuff
                            for i,otherPart in ipairs(parts) do
                                if otherPart and not Slua.IsNull(otherPart) then

                                    -- find matching field on other parts
                                    local otherBuilderFields = ModAssemblyLoaderBuilderLua.BuilderFieldHelper.GetBuilderFields(otherPart)
                                    for otherField in Slua.iter(otherBuilderFields) do
                                        if otherField.Name == field.Name then
                                            ModAssemblyLoaderBuilderLua.BuilderFieldHelper.SetFieldValue(otherPart, otherField, v)
                                        end
                                    end

                                    --read from properties
                                    otherPart:OnReadFromPropertiesNew()
                                end
                            end

                            --notify builder we changed current assembly
                            HBBuilder.Builder.ChangedCurrentAssembly()
                        end,
                    }

                    ------------------------------------------------

                    ------------------------------------------------
                    --setup array behaviour
                    ------------------------------------------------

                    self:SetupArrayBehaviour(part,property,parts,item)

                    ------------------------------------------------

                    --add to table
                    table.insert(ret,item)
                end
            end



        end

        ------------------------------------------------

        return ret
    end

    if type(target) =="userdata" and HBU.GetSystemType(target) == "Part[]" then

        --init vars
        local ret = {};
        local groupedParts = {}

        ------------------------------------------------
        --build list of grouped parts
        ------------------------------------------------

        local partGroup = {}
        local sumNameCheck = false
        local canMultiEdit = true
        local gotPart = false
        local partName = "Inspector"
        local firstPropertySumName = false

        for part in Slua.iter(target) do
            --if parts conforms to sumNameCheck then add them to the list
            if part and not Slua.IsNull(part) and not Slua.IsNull(part.properties) and #iter(part.properties) > 0 then
                local haveInspectorWorthyProperties = false
                for property in Slua.iter(part.properties) do
                    if property and property.hidden == false then haveInspectorWorthyProperties = true end
                end
                if haveInspectorWorthyProperties then
                    local propertySumName = part:GetPropertiesSumName()
                    if not firstPropertySumName then firstPropertySumName = propertySumName end
                    if propertySumName ~= firstPropertySumName then canMultiEdit = false; break; end
                    if not partGroup[propertySumName] then partGroup[propertySumName] = {} end
                    table.insert(partGroup[propertySumName],part)
                    gotPart = true
                    if not Slua.IsNull(part.gameObject:GetComponentInParent("PartContainer")) then partName = part.gameObject:GetComponentInParent("PartContainer").gameObject.name end
                end
            end
        end

        --dont go further if we dont have any parts in our group
        if not gotPart then return {} end

        ------------------------------------------------

        ------------------------------------------------
        --if we cantmulti edit, then add text property
        ------------------------------------------------

        if not canMultiEdit then
            local item = {
                name = "MultiEditFail",
                --info = "can not multi edit, refine your selection",
                uiType = "text",
                layout = {"text","can not multi edit, refine your selection","min",Vector2(0,30),"textalign",TextAnchor.MiddleCenter},
            }
            table.insert(ret,item)
            return ret
        end

        ------------------------------------------------

        ------------------------------------------------
        --convert part group properties to full property ui config
        ------------------------------------------------

        --create header item
        local headerItem = {
            name         = partName,
            tooltip      = "Part Proprties",
            uiType       = "headerProperty",
            layout       = {"min",Vector2(0,30)}
        }
        table.insert(ret,headerItem)

        --> foreach part in parts group
        for propertySumName,parts in pairs(partGroup) do
            local part = parts[1]

            --> foreach property of the first part in this group
            for property in Slua.iter(part.properties) do

                --ignore hidden properties
                if property and property.hidden == false then

                    ------------------------------------------------
                    --conver property to property ui config
                    ------------------------------------------------

                    local groupID = "properties"
                    if property.type == 13 then groupID = "Controls" end
                    if property.isInput    then groupID = "Inputs"   end
                    if property.isOutput   then groupID = "Outputs"  end

                    local item = {
                        name          = property.descriptiveName,
                        tooltip       = property.descriptiveName,
                        uiType        = self.PropertyTypeConversion[property.type],
                        minValue      = tonumber(property.minValue) or 0,
                        maxValue      = tonumber(property.maxValue) or 0,
                        options       = iter(property:GetOptions()),
                        groupID       = groupID,
                        matchingValue = self:GetPropertyMatchingValue(parts,property.pname),
                        --setup callback when it asks value ( return the property value of first part )
                        value      = function() return self:PropertyValueConversion(property,"tovalue") end,

                        --setup callback when we change value ( apply value on all parts in this group )
                        func       = function(v)

                        --foreach part >>
                        for i,gpart in ipairs(parts) do
                            if gpart and not Slua.IsNull(gpart) then

                            --property in part that matches this property
                            for prop in Slua.iter(gpart.properties) do
                                if prop.pname == property.pname then
                                    prop.value = self:PropertyValueConversion(prop,"tostring",v)
                                end
                            end

                            --read from properties
                            gpart:OnReadFromPropertiesNew()

                            end
                        end

                        --notify builder we changed current assembly
                        HBBuilder.Builder.ChangedCurrentAssembly()

                        end,
                    }

                    ------------------------------------------------

                    ------------------------------------------------
                    --setup array behaviour
                    ------------------------------------------------

                    self:SetupArrayBehaviour(part,property,parts,item)

                    ------------------------------------------------

                    --add to table
                    table.insert(ret,item)
                end
            end
        end
        return ret
    end
end

function ModAssemblyLoaderBuilderLua:FieldValueConversion(fieldInfo,mode,v)
    if not self then Debug.LogError("ModAssemblyLoaderBuilderLua:PropertyValueConversion: self is nil") return end

    local uiType = self.PropertyTypeConversion[property.type]

    --tostring the values
    if mode == "tostring" then
        if uiType == "keyProperty" then
            return v:ToDataString()
        end
        return tostring(v) or ""
    end

    --cast to correct value based on ui type
    if mode == "tovalue" then

        --booleans
        if uiType == "boolProperty" then
            if property.value == "true" or property.value == "True" then return true else return false end
        end

            --numbers
        if  uiType == "intProperty" or
            uiType == "floatProperty" or
            uiType == "doubleProperty" or
            uiType == "sliderProperty" then

            return tonumber(property.value) or 0

        end

            --string
        if  uiType == "textProperty" or
            uiType == "dialogeProperty" then

            return property.value or ""
        end

            --key
        if uiType == "keyProperty" then
            return HBU.CreateUnregisteredKey(property.value)
        end

    end

    --return if not converted
    return property.value
end

ModAssemblyLoaderBuilderLua.FieldTypeConversion = {
  ["System.Boolean"]  = "boolProperty",
  ["System.Int32"]  = "intProperty",
  ["System.Single"]  = "floatProperty",
  ["System.Double"]  = "doubleProperty",
  ["System.String"]  = "stringProperty",
  [5]  = "stringProperty",
  [6]  = "stringProperty",
  [7]  = "dialogProperty",
  [8]  = "sliderProperty",
  [9]  = "inputProperty",
  [10] = "outputProperty",
  [11] = "buttonProperty",
  [12] = "enumProperty",
  [13] = "keyProperty",
  [14] = "enumProperty",
}

return ModAssemblyLoaderBuilderLua
