﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using HBS;
using HarmonyLib;

namespace ModAssemblyLoader
{

    // adds mod type lookup handler
    public static class Modding
    {
        public static Dictionary<string, Type> modAssemblies = new Dictionary<string, Type>();
        public static void Inject(Type modType)
        {
            modAssemblies[modType.Name] = modType;
        }
    }

    #region serializer_patches

    [HarmonyPatch(typeof(HBS.Serializer.GameObjects), "ReadGameObjectStructureChunk")]
    public static class ReadGameObjectStructureChunk_Patch
    {
        // patch in new method
        static bool Prefix(int prefix, Reader r, GameObject root, out GameObject outRoot, GameObject[] objs, int fromIndex, int length)
        {
            outRoot = root;

            HBS.Serializer.currentRoot = root;//set current root ( UnserializePath uses this global var ) 

            for (var i = fromIndex; i < fromIndex + length; i++)
            {
                objs[i] = new GameObject("Child" + i.ToString());
                if (i == 0 && root == null)
                {
                    root = objs[i];
                    HBS.Serializer.currentRoot = root; //set current root ( UnserializePath uses this global var )
                    outRoot = root;
                    objs[i].SetActive(false);
                    objs[i].transform.parent = root.transform;
                }
                else
                {
                    var parentFromPath = HBS.Serializer.UnserializePath(r);
                    if (parentFromPath != null)
                    {
                        objs[i].transform.SetParent((Transform)parentFromPath);
                    }
                    else
                    {
                        Debug.LogError("coudnt find path for " + objs[i].ToString());
                        objs[i].transform.parent = HBS.Serializer.currentRoot.transform;
                    }
                }
                try
                {
                    var componentCount = (int)r.Read();
                    for (var ii = 0; ii < componentCount; ii++)
                    {
                        var typeName = (string)r.Read();
                        Type ctype;
                        if (Modding.modAssemblies.ContainsKey(typeName))
                        {
                            ctype = Modding.modAssemblies[typeName];
                        }
                        else
                        {
                            ctype = Type.GetType(typeName + ",Assembly-CSharp");
                        }
                        if (ctype == null) { ctype = Type.GetType(typeName + ",UnityEngine"); }
                        if (ctype == null) { ctype = Type.GetType(typeName + ",UnityEngine.UI"); }
                        if (ctype == null) { ctype = Type.GetType(typeName + ",HBNetworking"); }
                        if (ctype != typeof(Transform))
                        {
                            if (objs[i].AddComponent(ctype) == null)
                            {
                                Debug.Log("failed to add " + typeName + ", adding MissingClass instead to preserve object hierarchy");
                                objs[i].AddComponent<MissingClass>();
                            }
                        }
                    }
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning(e.ToString());
                }
            }
            return false; // skip old serializer method completely
        }
    }

    [HarmonyPatch(typeof(HBS.Serializer.GameObjects), "ReadComponentChunk")]
    public static class ReadComponentChunk_Patch
    {
        // patch in new method
        static bool Prefix(int prefix, Reader r, GameObject root, int fromIndex, int length, int componentCount, Component[] comps)
        {

            HBS.Serializer.currentRoot = root;//set current root ( UnserializePath uses this global var )       

            if (prefix > 0)
            {

                for (var ii = fromIndex; ii < fromIndex + length; ii++)
                {
                    var typeName = (string)r.Read();
                    var compData = (byte[])r.Read();
                    try
                    {
                        var r2 = new Reader(compData);

                        Type ctype;
                        if (Modding.modAssemblies.ContainsKey(typeName))
                        {
                            ctype = Modding.modAssemblies[typeName];
                        } else
                        {
                            ctype = Type.GetType(typeName + ",Assembly-CSharp");
                        }
                        if (ctype == null) { ctype = Type.GetType(typeName + ",UnityEngine"); }
                        if (ctype == null) { ctype = Type.GetType(typeName + ",UnityEngine.UI"); }
                        if (ctype == null) { ctype = Type.GetType(typeName + ",HBNetworking"); }
                        var c = comps[ii];//objs[i].GetComponent(ctype);
                                          //Debug.Log(comps[ii].GetType());
                                          //Debug.Log(ctype);
                                          //Debug.Log(typeName);
                                          //Debug.Log(ctype == comps[ii].GetType());

                        HBS.Serializer.Unserialize(r2, ctype, (object)c);
                        r2.Close();
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogWarning(e.ToString());
                    }
                }

            }
            else
            {

                for (var ii = fromIndex; ii < fromIndex + length; ii++)
                {
                    var typeName = (string)r.Read();
                    var ctype = Type.GetType(typeName);
                    var c = comps[ii];
                    HBS.Serializer.Unserialize(r, ctype, (object)c);
                }

            }

            return false; // skip old serializer method completely
        }
    }
    #endregion
}
