﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;
using HarmonyLib;

namespace ModAssemblyLoader
{
    public class Injector
    {
        public static void InjectSerializerPatch()
        {
            Harmony.DEBUG = false;

            var harmony = new Harmony("ModAssemblyLoader");

            var assembly = Assembly.GetExecutingAssembly();
            harmony.PatchAll(assembly);

            AssemblyLoader.LoadAssemblies();
        }
    }
}
