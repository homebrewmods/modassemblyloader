-- ModAssemblyLoader by RhoSigma
ModAssemblyLoader = {}

ModAssemblyLoader.version = "1.0 15/03/2022"

function ModAssemblyLoader:Awake()
    local assmblyPath = HBU.GetLuaFolder().."/ModAssemblies"
    if not HBU.DirectoryExists(assmblyPath) then
        HBU.CreateDirectory(assmblyPath)
    end
    local reflection = Slua.GetClass("System.Reflection.Assembly")
    local path = HBU.GetLuaFolder().."/ModLua/ModAssemblyLoader/ModAssemblyLoader.dll"
    reflection.LoadFrom(path)
    local injector = Slua.CreateClass("ModAssemblyLoader.Injector")
    injector.InjectSerializerPatch()
end

return ModAssemblyLoader
